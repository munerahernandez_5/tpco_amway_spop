﻿using System.Web.Services;
using System.Collections.Generic;
using Newtonsoft.Json;
using Teleperformance.Helpers.Logger;
using System.Text;
using System.Web.Script.Services;
using Microsoft.Security.Application;
using System;
using BLL;

namespace ScreenPopAmway
{
    /// <summary>
    /// Summary description for WebMethods
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class WebMethods : System.Web.Services.WebService
    {
        [WebMethod(EnableSession = true)]
        public string ObtenerDatosEmpresario(string Identificacion)
        {
            clsDatosSpop Dataload = new clsDatosSpop();
            try
            {
                Dataload = Services.DB.ConsultarSpopData(Identificacion);
                Dataload.DiasAntiguedad = Services.clsUtils.Encontrardias(Dataload.Fechaingreso) + " días";
            }
            catch (Exception ex)
            {
                Services.clsGlobal.Log.RegisterEvent(FEventType.Exception, "ObtenerDatosEmpresario", ex, null, Environment.UserName);

            }
            return JsonConvert.SerializeObject(Dataload);
        }





    }
}
