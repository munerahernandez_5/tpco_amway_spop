﻿
using System;

namespace ScreenPopAmway
{
    public partial class Reload : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {               

                lblMessage.Text = "Reload Configurations OK";
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
            }
        }

    }
}