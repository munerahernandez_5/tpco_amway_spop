﻿using System;
using Teleperformance.Helpers.Logger;

namespace ScreenPopAmway
{
    public class Global : System.Web.HttpApplication
    {
        protected void Application_Start(object sender, EventArgs e)
        {
           
            System.Net.ServicePointManager.SecurityProtocol = (System.Net.SecurityProtocolType.Tls | (System.Net.SecurityProtocolType.Tls11 | (System.Net.SecurityProtocolType.Tls12 | System.Net.SecurityProtocolType.Ssl3)));                
            Services.clsGlobal.Initialize();            
           
        }

        protected void Session_Start(object sender, EventArgs e)
        {

        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {

        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {

        }

        protected void Application_Error(object sender, EventArgs e)
        {

        }

        protected void Session_End(object sender, EventArgs e)
        {

        }

        protected void Application_End(object sender, EventArgs e)
        {

        }

        protected void GetPathsOfAllDirectoriesAbove(object sender, EventArgs e)
        {

        }
    }
}