﻿using BLL;
using Microsoft.Security.Application;
using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ScreenPopAmway.Services;


namespace ScreenPopAmway
{
    public partial class Amway : System.Web.UI.Page
    {
        wsRecordingManager.wsRecordingService wsRecordingManager = new wsRecordingManager.wsRecordingService();
        wsRecordingManager.DTO wsRecordingReturn;
        static long[] arrayId;

        protected void Page_Load(object sender, EventArgs e)
        {
          
            try
            {

                if (!IsPostBack)
                {

                    string usuario = Sanitizer.GetSafeHtmlFragment(Request["usuario"]);
                    usuario = HttpUtility.UrlDecode(usuario);
                    string vdn = Sanitizer.GetSafeHtmlFragment(Request["vdn"]);
                    vdn = HttpUtility.UrlDecode(vdn);
                    string ani = Sanitizer.GetSafeHtmlFragment(Request["ani"]);
                    ani = HttpUtility.UrlDecode(ani);
                    string ucid = Sanitizer.GetSafeHtmlFragment(Request["ucid"]);
                    ucid = HttpUtility.UrlDecode(ucid);
                    string uui = Sanitizer.GetSafeHtmlFragment(Request["uui"]);
                    uui = HttpUtility.UrlDecode(uui);
                    lblUser.Text = usuario;
                    


                    if (uui != string.Empty)
                    {
                        try
                        {
                            ScreenPopCustomerData DatosLLamada = clsCallDatasevice.IVRCustomerData(uui);
                            txtMenuPrincipal.Value = DatosLLamada.CustomerDetail.opcionMenuPrincipal;
                            txtSubMenuPrincipal.Value = DatosLLamada.CustomerDetail.opcionMenuFinal;
                            txtClienteBusca.Value = DatosLLamada.CustomerDetail.numeroEmpresario;
                        }
                        catch (Exception ex)
                        {
                            Services.clsGlobal.Log.RegisterEvent(FEventType.Exception, "Error Consumiendo Call Data service:", ex, null, Environment.UserName);                       
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Services.clsGlobal.Log.RegisterEvent(FEventType.Exception, "Load page", ex, null, Environment.UserName);
            }

            
        }

        protected void btnMute_Click(object sender, EventArgs e)
        {
            try
            { 
                if (lblUser.Text!=string.Empty)
                {
                    wsRecordingReturn = wsRecordingManager.BlackOutStartV2(lblUser.Text);
                    arrayId= new long[] { wsRecordingReturn.resultCode };
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Success" + Guid.NewGuid().ToString(), "fMute();", true);
                    Services.clsGlobal.Log.RegisterEvent(FEventType.Info, "fMute" + " " + wsRecordingReturn.resultCode.ToString() + " " + wsRecordingReturn.resultMessage, null , null, Environment.UserName);
                }
            }
            catch (Exception ex)
            {
                Services.clsGlobal.Log.RegisterEvent(FEventType.Exception, "btnMute_Click", ex, null, Environment.UserName);
            }

        }

        protected void btnUnmute_Click(object sender, EventArgs e)
        {
            try
            {                
                if (arrayId.Length>0)
                {
                    wsRecordingManager.BlackOutStopV2(arrayId);
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Success" + Guid.NewGuid().ToString(), "fUnMute();", true);
                    Services.clsGlobal.Log.RegisterEvent(FEventType.Info, "fUnmute" + " " + wsRecordingReturn.resultCode.ToString() + " " + wsRecordingReturn.resultMessage, null, null, Environment.UserName);
                }
            }
            catch (Exception ex)
            {
                Services.clsGlobal.Log.RegisterEvent(FEventType.Exception, "btnUnmute_Click", ex, null, Environment.UserName);
            }

        }
    }
}