﻿function llenarListaAjax(nombreLista, valFiltro, defaultValue, ejecutarChange) {

    if (typeof defaultValue === 'undefined')
        valFiltro = "";

    var obj = new Object();
    obj.nombreLista = nombreLista;
    obj.valorfiltro = valFiltro;

    execMethod("./WebMethods.asmx/ConsultarLista", "POST", obj, function (resp) {
        var data = JSON.parse(resp.d);
        setSelectOptions($('#' + nombreLista), data, "Codigo", "Descripcion", defaultValue);
        if (ejecutarChange)
            $('#' + nombreLista).change();
    });
}

function execMethod(uri, verb, _data, funcSuccess) {
    $.ajax({
        type: verb,
        url: uri,
        data: JSON.stringify(_data),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: true,
        error: function (jqXHR, textStatus, errorThrown) {
            alert("Error- Status: " + textStatus + " jqXHR Status: " + jqXHR.status + " jqXHR Response Text:" + jqXHR.responseText);
            //alert('Ha ocurrido un error al tratar de realizar la acción, por favor contacte al administrador del sistema.', 'Error');
        },
        success: funcSuccess
    });
}

function setSelectOptions(selectElement, values, valueKey, textKey, defaultValue) {

    if (typeof selectElement === "string") {
        selectElement = $(selectElement);
    }

    selectElement.empty();

    if (typeof values === 'object') {
        if (values.length) {
            var type = typeof values[0];
            var html = "";

            if (type === 'object') {
                // values is array of hashes
                var optionElement = null;

                $.each(values, function () {
                    html += '<option value="' + this[valueKey] + '">' + this[textKey] + '</option>';
                });

            } else {
                // array of strings
                $.each(values, function () {
                    var value = this.toString();
                    html += '<option value="' + value + '">' + value + '</option>';
                });
            }
            selectElement.append(html);
        }

        // select the defaultValue is one was passed in
        if (typeof defaultValue !== 'undefined') {
            selectElement.children('option[value="' + defaultValue + '"]').attr('selected', 'selected');
        }
    }
    return false;
}

function setCkeckBoxOptions(selectElement, values, valueKey, textKey, defaultValue) {

    if (typeof selectElement === "string") {
        selectElement = $(selectElement);
    }

    selectElement.empty();

    if (typeof values === 'object') {
        if (values.length) {
            var type = typeof values[0];
            var html = "";

            if (type === 'object') {
                // values is array of hashes
                var optionElement = null;

                $.each(values, function () {
                    html += '<option value="' + this[valueKey] + '">' + this[textKey] + '</option>';
                });

            } else {
                // array of strings
                $.each(values, function () {
                    var value = this.toString();
                    html += '<option value="' + value + '">' + value + '</option>';
                });
            }
            selectElement.append(html);
        }

        // select the defaultValue is one was passed in
        if (typeof defaultValue !== 'undefined') {
            selectElement.children('option[value="' + defaultValue + '"]').attr('selected', 'selected');
        }
    }
    return false;
}

function fnTPAutocompleteInitV2(textBoxControl, textBoxCode, URL, functionNameToTrigger) {
    //init autocomplete  parameters
    $(textBoxControl).autocomplete({
        source: URL,
        minLength: 2,
        autoFocus: true,
        select: function (event, ui) {
            $(textBoxCode).val('');
            if (functionNameToTrigger !== '') {
                eval(functionNameToTrigger);
            }
            if (ui.item) {
                $(textBoxCode).val(ui.item.id);
                $(textBoxControl).val(ui.item.value);
                if (functionNameToTrigger !== '') {
                    eval(functionNameToTrigger);
                }
                return false;
            }
        }
    }); //end parameters

    //override keydown to special keys
    $(textBoxControl).keydown(function (event) {
        var keyCode = $.ui.keyCode;
        //144 NUM_LOCK
        if ((event.keyCode === keyCode.TAB) ||
            (event.keyCode === keyCode.ENTER) ||
            (event.keyCode === keyCode.SHIFT) ||
            (event.keyCode === keyCode.CONTROL) ||
            (event.keyCode === keyCode.CAPS_LOCK) ||
            (event.keyCode === keyCode.LEFT) ||
            (event.keyCode === keyCode.RIGHT) ||
            (event.keyCode === keyCode.HOME) ||
            (event.keyCode === keyCode.END) ||
            (event.keyCode === keyCode.INSERT) ||
            (event.keyCode === keyCode.ALT) ||
            (event.keyCode === keyCode.WINDOWS) ||
            (event.keyCode === keyCode.NUMPAD_ENTER) ||
            (event.keyCode === 144)
        ) {
            //nothing to do
        }
        else {
            $(textBoxCode).val('');
            if (functionNameToTrigger !== '') {
                eval(functionNameToTrigger);
            }
        }
    }); //end override keydown

};

//function to search all
function TPsearchAllitemsAutoCompleteV2(textBoxControl, textBoxCode, functionNameToTrigger) {
    $(textBoxCode).val('');
    $(textBoxControl).val('');
    if (functionNameToTrigger !== '') {
        eval(functionNameToTrigger);
    }
    $(textBoxControl).autocomplete("search", "...");
    $(textBoxControl).focus();
};

function fnDummy() {
    var d = new Date();
    return d.getTime();
}