﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Amway.aspx.cs" Inherits="ScreenPopAmway.Amway" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
    <meta http-equiv="Pragma" content="no-cache" />
    <meta http-equiv="Expires" content="0" />
    <title>ScreenPopAmway</title>
    <link rel="shortcut icon" href="data:image/x-icon;," type="image/x-icon" />
    <link href="Content/Amway.css" rel="stylesheet" />
    <link href="Content/bootstrap.min.css" rel="stylesheet" />
    <link href="Content/jquery-ui.css" rel="stylesheet" />
    <link href="Content/font-awesome.min.css" rel="stylesheet" />
    <link href="Content/Select2.min.css" rel="stylesheet" />
    <link href="css/datatables/dataTables.bootstrap.min.css" rel="stylesheet" />
    <link href="css/datatables/Responsive-2.2.6/responsive.bootstrap.min.css" rel="stylesheet" />
    <link href="css/datatables/ColReorder-1.5.2/colReorder.bootstrap.min.css" rel="stylesheet" />
    <link href="css/datatables/Buttons-1.6.5/buttons.bootstrap.min.css" rel="stylesheet" />
    <link href="Content/Style.css" rel="stylesheet" />
    <style type="text/css">
        .auto-style1 {
            width: 81px;
            height: 72px;
            margin-left: 0px;
        }
        .auto-style2 {
            width: 59px;
            height: 82px;
            margin-left: 0px;
        }
        .auto-style3 {
            width: 67px;
            height: 68px;
        }
        .auto-style4 {
            width: 25px;
            height: 32px;
        }
        .auto-style5 {
            width: 366px;
            height: 121px;
            margin-left: 0px;
        }
        .auto-style6 {
            width: 83px;
            height: 66px;
            margin-left: 0px;
        }
    </style>
</head>
<body>
    <br /> 
   <form id="frmAmway" runat="server" data-toggle="validator">
    <div class="container">
        <asp:ScriptManager ID="ScriptManager1" runat="server" AsyncPostBackTimeout="0" EnablePageMethods="true">
        </asp:ScriptManager>
        <br />       
        <div>
             <header>
                  <div class="header">
                      <div id="headAmway" runat="server" class="backgroundAmway">                        
                        <img src="images/LogoAmway.png" class="auto-style5" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                          <img src="images/Comentario.png" class="auto-style2" />
                          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br />
                          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                          <img src="images/icono.png" class="auto-style3" />
                          <img src="images/TelefonoRing.png" class="auto-style4" />                        
                    </div>
                 </div>
            </header>
        </div>

        <div style="width: 100%">           
            <div class="row">
                   <div class="col-sm-4">                        
                            <label class="control-label">User</label>
                            <asp:Label ID="lblUser" runat="server" />  
                     </div>
                    <div class="col-sm-4">
                        <asp:Label ID="lblInfo" Text="Información del Contacto" Visible="false" runat="server" /> 
                    </div>
                     <div class="col-sm-4">
                        <asp:Label ID="lblInfoContacto" Text="Información del Contacto" runat="server" ForeColor="#000000" BorderColor="Black" /> 
                    </div>
                  <div class="col-sm-12">
                          <div class="col-sm-3">
                                <div class="form-group" id="DocumentGroup">
                                            <label class="control-label" for="txtBuscarDocumento">Busqueda número empresario</label>
                                            <input id="txtIdDocumento" runat="server" type="text" class="hidden" />
                                            <div class="input-group">                                              
                                                 <input type="text" id="txtClienteBusca" runat="server" class="form-control onlyNumbers" maxlength="50" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" onkeypress="return fnKeySearchClient(event);" />                                                 
                                                <div class="input-group-btn">
                                                    <button id="btnSearchClient" class="btn btn-primary" runat="server">
                                                       <span class="fa fa-search"></span>
                                                    </button>
                                                    <%--<input type="button" id="btnSearchClient"  class="btn btn-primary" />--%>                                                   
                                                </div>
                                            </div>
                                </div>
                        </div>
                 </div>
             </div>

             <div class="row">
                <div class="col-sm-12">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label class="control-label" id="lblNombreContacto" for="txtNombreContacto">Nombre de Contacto</label>                            
                            <input type="text" id="txtNombreContacto" runat="server" class="form-control" disabled="disabled" />
                        </div>
                        <div class="form-group">
                            <label class="control-label" for="txt">Numero Empresario</label>                            
                            <input type="text" id="txtNumeroEmpresario" runat="server" class="form-control" disabled="disabled" />
                         </div>
                    </div>
                    <div class="col-sm-6">                        
                         <div class="form-group">
                            <label class="control-label" for="txtPais">Pais</label>                            
                            <input type="text" id="txtPais" runat="server" class="form-control" disabled="disabled" />
                        </div>                          
                         <div class="form-group">
                            <label class="control-label" for="txtTiempoAntiguedad">Tiempo Antiguedad</label>                            
                            <input type="text" id="txtTiempoAntiguedad" runat="server" class="form-control" disabled="disabled" />
                        </div>
                    </div>                                
                   
                </div>
            </div>
        </div>  
        
        <div class="row" align="center">
            <div class="col-sm-6">                
                <div class="form-group">
                    <label class="control-label" for="txtOpcionIVR">Opción IVR</label>                           
                </div>   
                <div class="col-sm-6">
                    <label class="control-label" for="txtMenuPrincipal">Menú Pricipal</label> 
                    <input type="text" id="txtMenuPrincipal" runat="server" class="form-control" disabled="disabled" />
                </div>
                <div class="col-sm-6">
                     <label class="control-label" for="txtSubMenuPrincipal">Sub-Menú</label> 
                    <input type="text" id="txtSubMenuPrincipal" runat="server" class="form-control" disabled="disabled" />
                </div>    
            </div>
            <div class="col-sm-6">
                 <div class="form-group">
                    <label class="control-label" for="txtOpcionIVR">BlackOut</label>                           
                </div>
                <div class="col-sm-6">
                    <asp:Button ID="btnMute" runat="server" Text="Mute" CssClass="btn btn-primary" OnClick="btnMute_Click"  />                    
                    <img id="imgMute" src="images/MicrofonoCerrado.png" class="auto-style6" />
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                         <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="btnMute" />                            
                        </Triggers>
                    </asp:UpdatePanel>
                </div>
                <div class="col-sm-6">
                    <asp:Button ID="btnUnmute" runat="server" Text="UnMute" CssClass="btn btn-primary" OnClick="btnUnmute_Click" />                    
                    <img id="imgUNMute" src="images/MicrofonoAbierto.png" class="auto-style6"  />
                    <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                         <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="btnUnmute" />                            
                        </Triggers>
                    </asp:UpdatePanel>
                </div>                
            </div>           
        </div> 
        <br />
        <br />
   </div>
  </form>
    <script src="Scripts/Amway.js"></script>
    <script src="Scripts/jquery-3.5.1.min.js" type="text/javascript"></script>
    <script src="Scripts/jquery-ui.js" type="text/javascript"></script>
    <script src="Scripts/bootstrap.min.js" type="text/javascript"></script>
    <!--Datatables-->
    <script src="Scripts/datatables/jquery.dataTables.min.js"></script>
    <script src="Scripts/datatables/dataTables.bootstrap.min.js"></script>
    <script src="Scripts/datatables/Responsive-2.2.6/dataTables.responsive.min.js"></script>
    <script src="Scripts/datatables/ColReorder-1.5.2/dataTables.colReorder.min.js"></script>
    <script src="Scripts/datatables/Buttons-1.6.5/dataTables.buttons.min.js"></script>
    <script src="Scripts/jqueryValidator/jquery.form-validator.min.js"></script>
    <!-- select2 -->
    <script src="Scripts/select2.min.js"></script>
    <!-- sweetalert -->
    <script src="Scripts/sweetalert.min.js"></script>
    <!-- Script -->
     <script type="text/javascript">
         $(document).ready(function ()
         {
            // Initiate form validation
             $.validate();

             fnSearchEmpresario();
             document.getElementById('btnUnmute').disabled=true; 

         });

         function fnSearchEmpresario()
         {
             if (document.getElementById("txtClienteBusca").value != "")
             {
                 var objDocumento = new Object();
                 objDocumento.Identificacion = document.getElementById("txtClienteBusca").value;
                 execMethod('WebMethods.asmx/ObtenerDatosEmpresario', 'POST', objDocumento, function (resp)
                 {
                     try {
                         var datosJson = JSON.parse(resp.d);
                         console.log(datosJson);
                         if (datosJson.Documento)
                         {
                             document.getElementById("txtNombreContacto").value = datosJson.NombreEmpresario;
                             document.getElementById("txtNumeroEmpresario").value = datosJson.Documento;
                             document.getElementById("txtPais").value = datosJson.Pais;
                             document.getElementById("txtTiempoAntiguedad").value = datosJson.DiasAntiguedad;
                         }
                         else
                         {                             
                             swal("Error", "No se encontraron datos para ese documento", "error");
                         }
                     }
                    catch (e) {
                        swal("Error", "Ocurrio un error consultando los datos del empresario", "error");
                    }

                 });
             }
         }

         function fMute()
         {
             document.getElementById('btnMute').disabled = true;
             document.getElementById('btnUnmute').disabled = false;   
             swal("MUTE", "Llamada en Mute", "success");
            return true;
         }

          function fUnMute()
         {
            document.getElementById('btnMute').disabled = false;
            document.getElementById('btnUnmute').disabled = true;    
            swal("UNMUTE", "Llamada en UnMute", "success");
            return true;
         }

     </script>
</body>
</html>
