﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Xml.Serialization;

namespace ScreenPopAmway.Services
{
    public class clsCallDatasevice
    {

        public static ScreenPopCustomerData IVRCustomerData(string UUI)
        {
            HttpClient Client;
            Client = new HttpClient { BaseAddress = new Uri(clsGlobal.CallDataService) };
            Client.DefaultRequestHeaders.Accept.Clear();
            Client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            string path = clsGlobal.UrlCallDataService + UUI;

            ScreenPopCustomerData detalleLlamada = null;
            HttpResponseMessage response = Client.GetAsync(path).Result;
            if (response.IsSuccessStatusCode)
            {
                detalleLlamada=response.Content.ReadAsAsync<ScreenPopCustomerData>().Result;
                if (detalleLlamada != null && detalleLlamada.AdditionalData1 != null)
                {
                    detalleLlamada.CustomerDetail = DeserializarDetalleCliente(detalleLlamada.AdditionalData1);
                }                  
            }
            else if (response.StatusCode == HttpStatusCode.NotFound)
            {
                throw new ApplicationException("NOTFOUND");
            }

            return detalleLlamada;
        }

        public static DetalleCliente DeserializarDetalleCliente(string jsonDetalleCliente)
        {
            return Newtonsoft.Json.JsonConvert.DeserializeObject<DetalleCliente>(jsonDetalleCliente);
        }

    }
}