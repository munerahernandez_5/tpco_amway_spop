﻿using BLL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ScreenPopAmway.Services
{
    public class clsUtils
    {
        public static string Encontrardias(DateTime fecha)
        {
            try
            {                
                TimeSpan days = DateTime.Now.Subtract(fecha);
                return days.Days.ToString();
            }
            catch (Exception ex)
            {
                Services.clsGlobal.Log.RegisterEvent(FEventType.Exception, "Encontrardias", ex, null, Environment.UserName);
                return "-1";

            }
        }

    }
}