﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ScreenPopAmway.Services
{
    public class ScreenPopCustomerData : CustomerData
    {
        private DetalleCliente _CustomerDetail;
        public DetalleCliente CustomerDetail
        {
            get
            {
                return _CustomerDetail;
            }
            set
            {
                _CustomerDetail = value;
            }
        }
    }
}