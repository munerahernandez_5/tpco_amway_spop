﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ScreenPopAmway.Services
{
   
        public class CustomerData
        {
        public string ANI { get; set; }
        public string AdditionalData1 { get; set; }
        public string AdditionalData10 { get; set; }
        public string AdditionalData11 { get; set; }
        public string AdditionalData12 { get; set; }
        public string AdditionalData2 { get; set; }
        public string AdditionalData3 { get; set; }
        public string AdditionalData4 { get; set; }
        public string AdditionalData5 { get; set; }
        public string AdditionalData7 { get; set; }
        public string AdditionalData8 { get; set; }
        public string AdditionalData9 { get; set; }
        public string AppName { get; set; }
        public string ApplicationId { get; set; }
        public string CallId { get; set; }
        public string DNIS { get; set; }
        public string DestinationNumber { get; set; }
        public string DocumentNumber { get; set; }
        public string DocumentType { get; set; }
        public string EndDate { get; set; }
        public string IpWAS { get; set; }
        public string JsonCustomerDetails { get; set; }
        public string SessionId { get; set; }
        public string StartDate { get; set; }
        public string Termination { get; set; }
        public string UCID { get; set; }
        public string UEC { get; set; }
        public string UUI { get; set; }


        }
}