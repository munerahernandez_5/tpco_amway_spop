﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ScreenPopAmway.Services
{
    public class DetalleCliente: CustomerData
    {
        public string paisEmpresario { get; set; }
        public string numeroEmpresario { get; set; }
        public string opcionMenuPrincipal { get; set; }
        public string prioridadEmpresario { get; set; }
        public string fechaIngresoEmpresario { get; set; }
        public string opcionMenuFinal { get; set; }
        public string nombreEmpresario { get; set; }
        public string paisLlamada { get; set; }
    }
}