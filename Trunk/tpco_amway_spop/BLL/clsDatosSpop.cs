﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public class clsDatosSpop
    {
        public string Documento { get; set; }
        public string NombreEmpresario { get; set; }
        public string Pais { get; set; }
        public DateTime Fechaingreso { get; set; }
        public string DiasAntiguedad { get; set; }

        public static clsDatosSpop ConsultarDataCargados(string Identificacion, string connection, bool encrypted)
        {
            DAL.Database.StoredProcedure sp = DAL.Database.Sps.tblCarga_SeleccionarDatos(Identificacion, null);
            DataSet ds = null;
            try
            {
                ds = clsUtils.GetDataSetSP(connection, encrypted, ref sp);
            }
            catch (Exception ex)
            {

                return null;
            }
            clsDatosSpop DataLoad = clsUtils.CargarObjeto(typeof(clsDatosSpop), ds.Tables[0]) as clsDatosSpop;
            return DataLoad;

        }
    }
}
