﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public class clsDatosExcel
    {
        public string MensajeValidacion { get; set; } = "";

        public static clsDatosExcel ValidarDatosInsertados(string connection, bool encrypted)
        {
            DAL.Database.StoredProcedure sp = DAL.Database.Sps.tblCarga_ValidarDatos(null);
            DataSet ds = null;
            try
            {
                ds = clsUtils.GetDataSetSP(connection, encrypted, ref sp);
            }
            catch (Exception ex)
            {

                return null;
            }
            clsDatosExcel DataLoad = clsUtils.CargarObjeto(typeof(clsDatosExcel), ds.Tables[0]) as clsDatosExcel;
            return DataLoad;

        }

        public static clsDatosExcel ValidarDatosFechaCorrecta(string connection, bool encrypted)
        {
            DAL.Database.StoredProcedure sp = DAL.Database.Sps.tblCarga_ValidarDatos_Fecha(null);
            DataSet ds = null;
            try
            {
                ds = clsUtils.GetDataSetSP(connection, encrypted, ref sp);
            }
            catch (Exception ex)
            {

                return null;
            }
            clsDatosExcel DataLoad = clsUtils.CargarObjeto(typeof(clsDatosExcel), ds.Tables[0]) as clsDatosExcel;
            return DataLoad;

        }

        public static void RemoveDataInfo(string connection, bool encrypted)
        {
            DAL.Database.StoredProcedure SP = DAL.Database.Sps.tblCargaTempDelete(null);
            clsUtils.ExecuteSP(connection, encrypted, ref SP, null);
        }

        public static void InsertDataInfo(string user, string connection, bool encrypted)
        {
            DAL.Database.StoredProcedure SP = DAL.Database.Sps.tblCargaInsert(user, null, null, null);
            clsUtils.ExecuteSP(connection, encrypted, ref SP, null);
        }

    }
}
