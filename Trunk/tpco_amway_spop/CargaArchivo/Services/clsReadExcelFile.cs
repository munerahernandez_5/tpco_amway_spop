﻿using ClosedXML.Excel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace CargaArchivo.Services
{
    public class clsReadExcelFile
    {
        public static DataTable GetDataFromExcel(string path, dynamic worksheet)
        {
            //Create a new DataTable.
            //Save the uploaded Excel file.
            DataTable dt = new DataTable();
            //FileStream fs = File.OpenRead(path);
            try
            {
                using (XLWorkbook workBook = new XLWorkbook(path))
                {
                    IXLWorksheet workSheet = workBook.Worksheet(worksheet);
                    bool firstRow = true;
                    foreach (IXLRow row in workSheet.Rows())
                    {
                        if (firstRow)
                        {
                            foreach (IXLCell cell in row.Cells())
                            {
                                if (!string.IsNullOrEmpty(cell.Value.ToString()))
                                {
                                    dt.Columns.Add(cell.Value.ToString());
                                }
                                else
                                {
                                    break;
                                }
                            }
                            firstRow = false;
                        }
                        else
                        {
                            int i = 0;
                            DataRow toInsert = dt.NewRow();
                            foreach (IXLCell cell in row.Cells(1, dt.Columns.Count))
                            {
                                try
                                {
                                    toInsert[i] = cell.Value.ToString();
                                }
                                catch
                                {

                                }
                                i++;
                            }
                            dt.Rows.Add(toInsert);
                        }
                    }
                    return dt;
                }


            }
            catch (Exception e)
            {

                return dt;
            }


        }
    }
}