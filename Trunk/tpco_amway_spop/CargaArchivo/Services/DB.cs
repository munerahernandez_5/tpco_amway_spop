﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CargaArchivo.Services
{
    public class DB
    {
        public static BLL.clsDatosExcel ValidarDatos()
        {
            BLL.clsDatosExcel result = BLL.clsDatosExcel.ValidarDatosInsertados(clsGlobal.dns, false);
            return result;
        }

        public static BLL.clsDatosExcel ValidarDatosFechaCorrecta()
        {
            BLL.clsDatosExcel result = BLL.clsDatosExcel.ValidarDatosFechaCorrecta(clsGlobal.dns, false);
            return result;
        }
    }
}