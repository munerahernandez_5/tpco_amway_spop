﻿using BLL;
using CargaArchivo.Models;
using CargaArchivo.Services;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CargaArchivo.Controllers
{
    public class CargaController : Controller
    {
        public string UserInfo = null;
        public ActionResult Index(string usuario)
        {
            if (!string.IsNullOrEmpty(usuario))
            {
                Services.clsUtils.strUser = usuario;
            }
            //return RedirectToAction("Home", "Index");

            return View("Index");
        }

        [HttpPost]
        public ActionResult LoadFile(HttpPostedFileBase postedFile)
        {
            try
            {
                string path = Server.MapPath(clsGlobal.DirUpload);
                string filepath = string.Empty;
                if (postedFile != null)
                {
                    if (string.IsNullOrEmpty(Services.clsUtils.strUser))
                    {
                        return Json(new { Value = false, Message = "No se ha especificado un usuario para la aplicación" }, JsonRequestBehavior.AllowGet);
                    }


                    clsGlobal.Log.RegisterEvent(FEventType.Info, "Paso la ejecución del usuario ", null, null, Environment.UserName);

                    if (!Directory.Exists(path))
                    {
                        Directory.CreateDirectory(path);
                    }
                    filepath = path + Path.GetFileName(postedFile.FileName);
                    int index = filepath.IndexOf(".xlsx");
                    filepath = filepath.Substring(0, index);
                    filepath = filepath + DateTime.Now.ToString("yyyy_MM_dd_mm_ss") + ".xlsx";

                    postedFile.SaveAs(System.IO.Path.Combine(filepath));

                    DataLoadInfo LoadFile = new DataLoadInfo();
                    if (LoadFile.ValidarParametros(postedFile, out string msg) == false)
                    {
                        return Json(new { Value = false, Message = msg }, JsonRequestBehavior.AllowGet);
                    }

                    clsGlobal.Log.RegisterEvent(FEventType.Info, "Pasó el validar parametros ", null, null, Environment.UserName);

                    DataTable xlsWBIPS = clsReadExcelFile.GetDataFromExcel(filepath, 1);

                    clsGlobal.Log.RegisterEvent(FEventType.Info, "leyó el excel ", null, null, Environment.UserName);

                    if (xlsWBIPS.Rows.Count <= 0)
                    {
                        return Json(new { Value = false, Message = "No se proceso correctamente el archivo" }, JsonRequestBehavior.AllowGet);
                    }

                    //Validamos que sea una fecha valida
                    if (LoadFile.ValidarFechaCorrectaIngresada(xlsWBIPS, out string Msg) == false)
                    {
                        return Json(new { Value = false, Message = Msg }, JsonRequestBehavior.AllowGet);
                    }

                    //Adicionamos los dias de antiguedad
                   // LoadFile.AdicionarDias(xlsWBIPS);

                    //Insertamos los datos temporales
                    LoadFile.InsertarDatosTemporales(xlsWBIPS, Services.clsUtils.strUser, out string message);
                    if (message != "")
                    {
                        return Json(new { Value = false, Message = message }, JsonRequestBehavior.AllowGet);
                    }


                    //Validamos los datos insertados,si esta configurado que si
                    if (clsGlobal.ValidarDatos == "SI")
                    {
                        string Validacion = LoadFile.ValidarDatosInsertados();
                        if (Validacion != string.Empty)
                        {
                            return Json(new { Value = false, Message = Validacion }, JsonRequestBehavior.AllowGet);
                        }
                        clsGlobal.Log.RegisterEvent(FEventType.Info, "Validó los datos", null, null, Environment.UserName);
                    }

                   
                                      


                    LoadFile.InsertarDatos(xlsWBIPS, Services.clsUtils.strUser, out string mensaje);
                    if (mensaje!="")
                    {
                        clsGlobal.Log.RegisterEvent(FEventType.Info, "Error insertando los datos", null, null, Environment.UserName);
                        return Json(new { Value = false, Message = mensaje }, JsonRequestBehavior.AllowGet);
                    }

                    clsGlobal.Log.RegisterEvent(FEventType.Info, "Insertó los registros ", null, null, Environment.UserName);

                    postedFile.InputStream.Dispose();


                }

            }
            catch (Exception ex)
            {
                return Json(new { Value = false, Message = "Error: " + ex.Message }, JsonRequestBehavior.AllowGet);
            }

            return Json(new { Value = true, Message = "Datos insertados correctamente" }, JsonRequestBehavior.AllowGet);

        }

        // GET: Carga/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Carga/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Carga/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Carga/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Carga/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Carga/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Carga/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
