﻿using BLL;
using CargaArchivo.Services;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace CargaArchivo.Models
{
    public class DataLoadInfo
    {
        public bool ValidarParametros(HttpPostedFileBase postedFile, out string msg)
        {
            msg = string.Empty;
            int contentLength = 0;
            bool result = false;
            string ArchivosPermitidos = clsGlobal.ArchivosPermitidos;
            try
            {
                if (Services.clsUtils.IsNumeric(clsGlobal.ContentLenght))
                {
                    contentLength = Convert.ToInt32(clsGlobal.ContentLenght);
                }
                //Validamos que haya un archivo cargado
                if ((postedFile != null) && (postedFile.ContentLength > 0))
                {
                    if (postedFile.FileName.EndsWith(ArchivosPermitidos))
                    {
                        //Validamos el tamaño del archivo
                        if (postedFile.ContentLength > contentLength)
                        {
                            msg = string.Format("El tamaño del archivo debe ser menor o igual {0}bytes", contentLength);
                            return result;
                        }
                    }
                    else
                    {
                        msg = string.Format("No se pudo cargar el archivo seleccionado, por favor seleccione un archivo .xlsx", contentLength / 1000);
                        return result;
                    }
                }

            }
            catch (Exception)
            {
                msg = string.Format("Error procesando el archivo", contentLength / 1000);
                return result;
            }
            result = true;
            return result;

        }

        public void InsertarDatosTemporales(DataTable dt, string User, out string msg)
        {
            try
            {
                msg = "";               
                int contentInsert = 0;
                string ConnectionString = clsGlobal.dns;
                BLL.clsDatosExcel.RemoveDataInfo(ConnectionString, false);
                clsGlobal.Log.RegisterEvent(FEventType.Info, "elimino registros ", null, null, Environment.UserName);
                if (Services.clsUtils.IsNumeric(clsGlobal.CantidadCarga))
                {
                    contentInsert = Convert.ToInt32(clsGlobal.CantidadCarga);
                }
                else
                {
                    contentInsert = 10000;
                }
                int contador = 0;
                int contadorMax = contador + contentInsert;

                int i = 1;
                DataTable newDt = dt.Clone();
                foreach (DataRow row in dt.Rows)
                {
                    if (i > contador & i <= contadorMax)
                    {
                        DataRow newRow = newDt.NewRow();
                        newRow.ItemArray = row.ItemArray;
                        newDt.Rows.Add(newRow);



                        if (i == contadorMax)
                        {
                            using (SqlBulkCopy sqlBulk = new SqlBulkCopy(ConnectionString))
                            {
                                sqlBulk.BulkCopyTimeout = 0;
                                sqlBulk.DestinationTableName = "tblCargaTemp";
                                sqlBulk.WriteToServer(newDt);
                            }
                            newDt = dt.Clone();
                            contador = contadorMax;
                            contadorMax = contadorMax + contentInsert;
                        }
                        if (i >= dt.Rows.Count & newDt.Rows.Count > 0)
                        {
                            using (SqlBulkCopy sqlBulk = new SqlBulkCopy(ConnectionString))
                            {
                                sqlBulk.BulkCopyTimeout = 0;
                                sqlBulk.DestinationTableName = "tblCargaTemp";
                                sqlBulk.WriteToServer(newDt);
                            }
                            i = dt.Rows.Count + 1;
                        }
                        i++;
                    }
                    else
                    {
                        contador = contadorMax;
                        contadorMax = contadorMax + contentInsert;
                    }


                }



            }
            catch (Exception e)
            {
                msg = e.Message;
                clsGlobal.Log.RegisterEvent(FEventType.Info, e.Message, null, null, Environment.UserName);
            }
        }

        public string ValidarDatosInsertados()
        {
            string result = "";
            try
            {
                clsDatosExcel DataValidation = new clsDatosExcel();
                DataValidation = Services.DB.ValidarDatos();
                if (DataValidation.MensajeValidacion != string.Empty)
                {
                    result = DataValidation.MensajeValidacion;
                }
            }
            catch (Exception ex)
            {
                clsGlobal.Log.RegisterEvent(FEventType.Info, ex.Message, null, null, Environment.UserName);
            }
            return result;

        }

        public bool ValidarFechaCorrectaIngresada(DataTable xlsWBIPS, out string msg)
        {

            msg = string.Empty;
            bool result = false;
            int fila = 1;
            try
            {
                foreach (DataRow item in xlsWBIPS.Rows)
                {
                    if (!EsFecha(item.ItemArray[2].ToString()))
                    {
                        if (msg != string.Empty)
                        {
                            msg += ", ";
                        }

                        msg = string.Format("El campo Aplicacion en la fila {0}, no es una fecha valida", fila);
                    }
                    fila++;
                }
                if (string.IsNullOrEmpty(msg))
                {
                    result = true;
                }
                return result;
            }
            catch (Exception ex)
            {
                clsGlobal.Log.RegisterEvent(FEventType.Info, ex.Message, null, null, Environment.UserName);
            }
            return result;

        }


        public void AdicionarDias(DataTable xlsWBIPS)
        {
            try
            {
                int fila = 0;
                xlsWBIPS.Columns.Add("DiasAntiguedad");
                DataRow _ravi = xlsWBIPS.NewRow();
                foreach (DataRow item in xlsWBIPS.Rows)
                {
                    xlsWBIPS.Rows[fila]["DiasAntiguedad"]= Encontrardias(item.ItemArray[2].ToString());                 
                    fila = fila + 1;
                }
            }
            catch (Exception ex)
            {
                clsGlobal.Log.RegisterEvent(FEventType.Info, ex.Message, null, null, Environment.UserName);
            }

        }

        public static Boolean EsFecha(String fecha)
        {
            try
            {
                DateTime.Parse(fecha);
                return true;
            }
            catch
            {
                return false;
            }
        }

       public string ValidarFechaCorrecta()
        {
            string result = "";
            try
            {
                clsDatosExcel DataValidation = new clsDatosExcel();
                DataValidation = Services.DB.ValidarDatosFechaCorrecta();
                if (DataValidation.MensajeValidacion != string.Empty)
                {
                    result = DataValidation.MensajeValidacion;
                }
            }
            catch (Exception ex)
            {
                clsGlobal.Log.RegisterEvent(FEventType.Info, ex.Message, null, null, Environment.UserName);
            }
            return result;

        }

        public void InsertarDatos(DataTable dt, string User, out string msg)
        {
            try
            {
                msg = "";
                string ConnectionString = clsGlobal.dns;                
                BLL.clsDatosExcel.InsertDataInfo(User, ConnectionString, false);
            }
            catch (Exception e)
            {
                msg = e.Message;
                clsGlobal.Log.RegisterEvent(FEventType.Info, e.Message, null, null, Environment.UserName);
            }

        }

        public static string Encontrardias(string fecha)
        {
            try
            {
                DateTime Fecha = DateTime.Parse(fecha);
                TimeSpan days = DateTime.Now.Subtract(Fecha);
                return days.Days.ToString();
            }
            catch (Exception ex)
            {
                Services.clsGlobal.Log.RegisterEvent(FEventType.Exception, "Encontrardias", ex, null, Environment.UserName);
                return "-1";

            }
        }

    }
}