

/****** Object:  Table [dbo].[tblCarga]    Script Date: 21/06/2021 8:27:16 p. m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[tblCarga](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[uuiCarga] [uniqueidentifier] NOT NULL,
	[dtTimeStamp] [datetime] NOT NULL,
	[strUsuario] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_tblCarga] PRIMARY KEY CLUSTERED 
(
	[uuiCarga] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[tblCarga] ADD  CONSTRAINT [DF_tblCarga_uuiCarga]  DEFAULT (newid()) FOR [uuiCarga]
GO

/****** Object:  Table [dbo].[tblCarga_Detalles]    Script Date: 21/06/2021 8:27:56 p. m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[tblCarga_Detalles](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[strNumeroEmpresario] [varchar](50) NOT NULL,
	[strNombreEmpresario] [varchar](250) NULL,
	[dtFechaIngreso] [datetime] NULL,
	[strPais] [varchar](50) NULL,
	[dtmFechaCreacion] [datetime] NOT NULL,
	[uuiCarga] [uniqueidentifier] NOT NULL,
	[strUsuario] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_tblCarga_Detalles] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[tblCarga_Detalles]  WITH CHECK ADD  CONSTRAINT [FK_tblCarga_Detalles_tblCarga] FOREIGN KEY([uuiCarga])
REFERENCES [dbo].[tblCarga] ([uuiCarga])
GO

ALTER TABLE [dbo].[tblCarga_Detalles] CHECK CONSTRAINT [FK_tblCarga_Detalles_tblCarga]
GO


/****** Object:  Table [dbo].[tblCargaTemp]    Script Date: 21/06/2021 8:28:24 p. m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[tblCargaTemp](
	[strNumeroEmpresario] [varchar](50) NOT NULL,
	[strNombreEmpresario] [varchar](250) NULL,
	[strAplicacion] [varchar](50) NULL,
	[strPais] [varchar](50) NOT NULL,
	[dtDate] [datetime] NOT NULL,
	[Id] [int] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [PK_tblCargaTemp] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[tblCargaTemp] ADD  CONSTRAINT [DF_tmp_dtDate]  DEFAULT (getutcdate()) FOR [dtDate]
GO


IF NOT EXISTS(SELECT * FROM sys.indexes WHERE name = 'idx_strNumeroEmpresario' AND object_id = OBJECT_ID('tblCarga_Detalles'))
BEGIN
	CREATE NONCLUSTERED INDEX [idx_UCID] ON [dbo].[tblCarga_Detalles]
	(
		[strNumeroEmpresario] ASC
	)
	INCLUDE ( 
		[strNombreEmpresario],	
		[dtFechaIngreso],
		[strPais]
	) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
END
GO

--Procedimiento
IF object_id('sel_Empresario_CargaDetalle') IS NOT NULL
    DROP PROCEDURE sel_Empresario_CargaDetalle
GO
CREATE PROCEDURE [dbo].[sel_Empresario_CargaDetalle]
	@NumeroEmpresario varchar(50)
AS
BEGIN

	SET NOCOUNT ON;

	SELECT [strNombreEmpresario], [dtFechaIngreso], [strPais]
	FROM tblCarga_Detalles
	WHERE strNumeroEmpresario = @NumeroEmpresario

END
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Juan Camilo M�nera Hernandez>
-- Create date: <2021-06-08>
-- Description:	<Procedimiento encargado de validar los datos temporales insertados >
-- =============================================
CREATE PROCEDURE [dbo].[tblCarga_ValidarDatos]		
AS
BEGIN
	
	DECLARE @intFila AS INT;
	DECLARE @strCampo AS VARCHAR(50);

	BEGIN TRY

		SELECT TOP 1 @intFila=Id,@strCampo='NumeroEmpresario' 
		FROM [dbo].[tblCargaTemp]
		WHERE LEN(strNumeroEmpresario)=0

		SELECT TOP 1 @intFila=Id,@strCampo='NombreEmpresario' 
		FROM [dbo].[tblCargaTemp]
		WHERE LEN(strNombreEmpresario)=0

		SELECT TOP 1 @intFila=Id,@strCampo='Aplicacion' 
		FROM [dbo].[tblCargaTemp]
		WHERE LEN(strAplicacion)=0

		SELECT TOP 1 @intFila=Id,@strCampo='Pais' 
		FROM [dbo].[tblCargaTemp]
		WHERE LEN(strPais)=0

		IF @intFila>0
		BEGIN
			SELECT	'Error en la Fila: ' + CONVERT(VARCHAR,@intFila) + ', Campo: ' + @strCampo + ' es requerido' AS 'MensajeValidacion'

		END
		ELSE
		BEGIN
			SELECT	'' AS 'MensajeValidacion'

		END

		


		END TRY

	BEGIN CATCH 
		--Si se presenta error
		--IF @@TRANCOUNT>0  ROLLBACK TRANSACTION;

		--SET @OUT_intError = ERROR_NUMBER();
		--SET @OUT_strError = ERROR_MESSAGE();
	END CATCH
END

/****** Object:  StoredProcedure [dbo].[tblCargaTempDelete]    Script Date: 21/06/2021 8:33:42 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Juan Camilo Munera Hernandez>
-- Create date: <2021-06-09>
-- Description:	<Eliminar los datos temporales>
-- =============================================
CREATE PROCEDURE [dbo].[tblCargaTempDelete]
AS
BEGIN
	Truncate Table [tblCargaTemp]
END
GO

/****** Object:  StoredProcedure [dbo].[tblCargaInsert]    Script Date: 21/06/2021 8:34:02 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Juan Camilo Munera>
-- Create date: <2021/06/09>
-- Description:	<Insert into the tables tblCarga and tblCarga_Detalles>
-- =============================================
CREATE PROCEDURE [dbo].[tblCargaInsert]
	@strUser Nvarchar(50)
	,@OUT_intError AS INT OUTPUT 
	,@OUT_strError AS VARCHAR(255) OUTPUT
AS
BEGIN
	DECLARE @tblCarga as TABLE (strNumeroEmpresario nvarchar(50), strNombreEmpresario nvarchar(250), strAplicacion nvarchar(50), strPais nvarchar(50));
	SET @OUT_intError=0;
	SET @OUT_strError='';
	DECLARE @uuiCarga AS uniqueidentifier;
	DECLARE @uuiCargaLast AS uniqueidentifier;
	DECLARE @ids table(id uniqueidentifier);
	DECLARE @intMaxID AS INT;

	BEGIN TRY
		INSERT INTO @tblCarga(strNumeroEmpresario, strNombreEmpresario, strAplicacion, strPais)
		SELECT T.strNumeroEmpresario, T.strNombreEmpresario, T.strAplicacion, T.strPais
		FROM tblCargaTemp T	

		SET @intMaxID=(SELECT MAX(Id) FROM tblCarga)
		SET @uuiCargaLast=(SELECT uuiCarga FROM tblCarga WHERE id=@intMaxID)

		INSERT INTO tblCarga(strUsuario, dtTimeStamp)
		OUTPUT INSERTED.uuiCarga into @ids
		VALUES (@strUser, GETUTCDATE())

		SET @uuiCarga=(SELECT id FROM @ids )

		DELETE tblCarga_Detalles
		WHERE uuiCarga=@uuiCargaLast

		UPDATE @tblCarga
		SET strAplicacion=REPLACE(strAplicacion,' 12:00:00 a.m.','')

		

		INSERT INTO tblCarga_Detalles(strNumeroEmpresario, strNombreEmpresario, dtFechaIngreso, strPais, dtmFechaCreacion, uuiCarga,strUsuario)
		SELECT C.strNumeroEmpresario, C.strNombreEmpresario, CONVERT (datetime,strAplicacion,103), C.strPais, GETUTCDATE(), @uuiCarga,@strUser
		FROM @tblCarga C


	END TRY

	BEGIN CATCH 
		--Si se presenta error
		--IF @@TRANCOUNT>0  ROLLBACK TRANSACTION;

		SET @OUT_intError = ERROR_NUMBER();
		SET @OUT_strError = ERROR_MESSAGE();
	END CATCH


END
GO

/****** Object:  StoredProcedure [dbo].[tblCarga_SeleccionarDatos]    Script Date: 21/06/2021 8:35:42 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Juan Camilo Munera H>
-- Create date: <2021-06-10>
-- Description:	<Obtiene los datos ingresados busca por documento>
-- =============================================
CREATE PROCEDURE [dbo].[tblCarga_SeleccionarDatos] 
	@strNumeroEmpresario AS NVARCHAR(50)
AS
BEGIN
	SELECT D.strNumeroEmpresario AS 'Documento',
	D.strNombreEmpresario AS 'NombreEmpresario',
	D.strPais AS 'Pais',
	D.dtFechaIngreso AS 'Fechaingreso'	
	FROM tblCarga_Detalles D 
	WHERE D.strNumeroEmpresario=@strNumeroEmpresario
END