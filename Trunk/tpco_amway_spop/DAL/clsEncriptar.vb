﻿Option Strict Off

Imports System
Imports System.Security.Cryptography
Imports System.IO
Imports System.Text
Imports System.Security.Principal
Imports Microsoft.VisualBasic

Public Class clsEncriptar
    'Función para encriptar el login
    'falta reemplazar por sha1
    Public Shared Function mEncriptar(ByVal A As String) As String
        Dim I As Integer
        Dim N As Long
        Dim h As String
        For I = 1 To Len(A)
            N = N + Asc(Mid(A, I, 1))
            N = (N * 1717 + 1717) Mod 1048576
        Next
        For I = 1 To 7
            N = (N * 997 + 997) Mod 1048576
        Next
        h = Right("0000" & Hex(N), 4)
        For I = 1 To Len(A)
            N = N + Asc(Mid(A, I, 1))
            N = (N * 997 + 997) Mod 1048576
        Next
        For I = 1 To 7
            N = (N * 1717 + 1717) Mod 1048576
        Next
        mEncriptar = h & Right("0000" & Hex(N), 4)
    End Function

End Class

Public Class clsEncriptaDesEncripta
    Private Shared des As TripleDESCryptoServiceProvider = New TripleDESCryptoServiceProvider

    Private Shared ReadOnly Property Key() As Byte()
        Get
            '24 bytes para 3des
            '------------------------------123456789012345678901234-----
            Return Encoding.UTF8.GetBytes("7sw.;&+|/(q=120*.Z<>+'|=")
        End Get
    End Property

    Private Shared ReadOnly Property Vector() As Byte()
        Get
            '24 bytes para 3des 
            '------------------------------123456789012345678901234-----
            Return Encoding.UTF8.GetBytes("#23(.*;|*'<Y>Ooa/(?+=$%a")
        End Get
    End Property

    Public Shared Sub Encrypt(ByVal Text() As Byte, ByRef salida() As Byte)
        Call Transform(Text, des.CreateEncryptor(Key, Vector), salida)
    End Sub

    Public Shared Sub Decrypt(ByVal encryptedText() As Byte, ByRef salida() As Byte)
        Call Transform(encryptedText, des.CreateDecryptor(Key, Vector), salida)
    End Sub

    Private Shared Sub Transform(ByVal Text() As Byte, ByVal CryptoTransform As ICryptoTransform, ByRef salida() As Byte)
        Dim stream As MemoryStream = New MemoryStream
        Dim cryptoStream As CryptoStream =
         New CryptoStream(stream, CryptoTransform,
                           CryptoStreamMode.Write)
        cryptoStream.Write(Text, 0, Text.Length)
        cryptoStream.FlushFinalBlock()
        stream.Close()
        salida = stream.ToArray
    End Sub

    Public Shared Function ByteArrayToString(ByVal ba() As Byte) As String
        Dim hex As StringBuilder
        hex = New StringBuilder(ba.Length * 2)
        For Each b As Byte In ba
            hex.AppendFormat("{0:x2}", b)
        Next
        Return hex.ToString()
    End Function

    Public Shared Function StringToByteArray(ByVal hex As String) As Byte()
        Dim NumberChars As Integer
        Dim intI As Integer
        Dim bytes() As Byte
        NumberChars = hex.Length
        ReDim bytes((NumberChars / 2) - 1)
        For intI = 1 To NumberChars / 2
            bytes(intI - 1) = Convert.ToByte(Mid(hex, intI * 2 - 1, 2), 16)
        Next
        Return bytes
    End Function
End Class

Public Class RSACrypto
    Private _sp As RSACryptoServiceProvider

    Public Function ExportParameters(ByVal includePrivateParameters As Boolean) As RSAParameters
        Return _sp.ExportParameters(includePrivateParameters)
    End Function

    Public Sub InitCrypto(ByVal keyFileName As String)
        Dim cspParams As New CspParameters()
        cspParams.Flags = CspProviderFlags.UseMachineKeyStore
        ''To avoid repeated costly key pair generation
        _sp = New RSACryptoServiceProvider(cspParams)
        'Dim path As String = keyFileName
        'Dim reader As System.IO.StreamReader = New StreamReader(path)
        'Dim data As String = reader.ReadToEnd()
        '_sp.FromXmlString(Data)

        _sp.FromXmlString(keyFileName)
    End Sub

    Public Function Encrypt(ByVal txt As String) As Byte()
        Dim result As Byte()
        Dim enc As New ASCIIEncoding()
        Dim numOfChars As Integer = enc.GetByteCount(txt)
        Dim tempArray As Byte() = enc.GetBytes(txt)
        result = _sp.Encrypt(tempArray, False)

        Return result
    End Function

    Public Function Decrypt(ByVal txt As Byte()) As Byte()
        Dim result As Byte()
        Try
            result = _sp.Decrypt(txt, False)
        Catch ex As Exception
            result = txt
        End Try

        Return result
    End Function
End Class

Public Class StringHelper

    Public Shared Function HexStringToBytes(ByVal hex As String) As Byte()
        If hex.Length = 0 Then
            Return New Byte() {0}
        End If
        If hex.Length Mod 2 = 1 Then
            hex = "0" & hex
        End If
        Dim result As Byte() = New Byte(hex.Length / 2 - 1) {}

        For i As Integer = 0 To hex.Length / 2 - 1
            result(i) = Byte.Parse(hex.Substring(2 * i, 2), System.Globalization.NumberStyles.AllowHexSpecifier)
        Next

        Return result
    End Function

    Public Shared Function BytesToHexString(ByVal input As Byte()) As String
        Dim hexString As New StringBuilder(64)

        For i As Integer = 0 To input.Length - 1
            hexString.Append([String].Format("{0:X2}", input(i)))
        Next
        Return hexString.ToString()
    End Function

    Public Shared Function BytesToDecString(ByVal input As Byte()) As String
        Dim decString As New StringBuilder(64)

        For i As Integer = 0 To input.Length - 1
            decString.Append([String].Format(IIf(i = 0, "{0:D3}", "-{0:D3}"), input(i)))
        Next
        Return decString.ToString()
    End Function

    ' Bytes are string
    Public Shared Function ASCIIBytesToString(ByVal input As Byte()) As String
        Dim enc As System.Text.ASCIIEncoding = New ASCIIEncoding()
        Return enc.GetString(input)
    End Function
    Public Shared Function UTF16BytesToString(ByVal input As Byte()) As String
        Dim enc As System.Text.UnicodeEncoding = New UnicodeEncoding()
        Return enc.GetString(input)
    End Function
    Public Shared Function UTF8BytesToString(ByVal input As Byte()) As String
        Dim enc As System.Text.UTF8Encoding = New UTF8Encoding()
        Return enc.GetString(input)
    End Function

    ' Base64
    Public Shared Function ToBase64(ByVal input As Byte()) As String
        Return Convert.ToBase64String(input)
    End Function

    Public Shared Function FromBase64(ByVal base64 As String) As Byte()
        Return Convert.FromBase64String(base64)
    End Function
End Class