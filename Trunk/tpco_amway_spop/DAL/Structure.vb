﻿Imports System.Data.SqlClient

Namespace Database
    '--Clase encargada de procesar los procedimientos almacenados de la base de datos--'
    Public Class StoredProcedure
        Public Property Command As SqlCommand
        Public Property OutputValues As List(Of Object)
        Private FConnection As clsDAL
        Private FCloseConnection As Boolean = False
        Private FProcedure_Name As String = String.Empty

        Public Sub New(ByVal Procedure_Name As String, Optional ByVal Connection As clsDAL = Nothing)
            If Not Connection Is Nothing Then
                FConnection = Connection
                FCloseConnection = False
            Else
                FConnection = New clsDAL
                FCloseConnection = True
            End If
            FProcedure_Name = Procedure_Name
            Command = New SqlCommand
            Command.CommandType = CommandType.StoredProcedure
        End Sub

        Public Sub New(ByVal Procedure As StoredProcedure)
            If Procedure.FConnection Is Nothing Then
                FConnection = New clsDAL
                FCloseConnection = True
            End If
            FProcedure_Name = Procedure.FProcedure_Name
            Command = Procedure.Command
            Command.CommandType = CommandType.StoredProcedure
        End Sub

        Public Function GetDataSet() As DataSet
            Dim DS As New DataSet
            Try
                'Abrimos la conexión a la base de datos.
                If Not FConnection.bolAbrio Then
                    'If Not FConnection.mAbrirBasedeDatos(ConnectionString) Then
                    If Not FConnection.mAbrirBasedeDatos(clsUtils.DSN, False) Then
                        'Si no se abre, recorremos los errores y lanzamos la excepción
                        If FConnection.mCantidadErrores > 0 Then
                            Dim Errores As String = String.Empty
                            For I As Integer = 1 To CInt(FConnection.mCantidadErrores - 1)
                                If Trim(FConnection.mDescripcionError(I)) <> String.Empty Then
                                    Errores = Errores & FConnection.mDescripcionError(I).ToString & vbCrLf
                                End If
                            Next
                            If Trim(Errores) <> String.Empty Then
                                Throw New Exception(Errores)
                            End If
                        End If
                    End If
                End If

                Dim LstParameters As New List(Of SqlClient.SqlParameter)
                For I As Integer = 0 To Command.Parameters.Count - 1
                    LstParameters.Add(New SqlParameter(Command.Parameters(I).ParameterName, Command.Parameters(I).SqlDbType, Command.Parameters(I).Size))
                    LstParameters(LstParameters.Count - 1).Value = Command.Parameters(I).Value
                    LstParameters(LstParameters.Count - 1).Direction = Command.Parameters(I).Direction
                Next
                FConnection.mRST(FProcedure_Name, LstParameters.ToArray)

                If FConnection.mCantidadErrores > 0 Then
                    Dim Errores As String = String.Empty
                    For I As Integer = 1 To CInt(FConnection.mCantidadErrores - 1)
                        If Trim(FConnection.mDescripcionError(I)) <> String.Empty Then
                            Errores = Errores & FConnection.mDescripcionError(I).ToString & vbCrLf
                        End If
                    Next
                    If Trim(Errores) <> String.Empty Then
                        Throw New Exception(Errores)
                    End If
                End If
                DS = FConnection.GetDataSet
                OutputValues = New List(Of Object)
                For I As Integer = 0 To LstParameters.Count - 1
                    If LstParameters(I).Direction = ParameterDirection.Output Then
                        OutputValues.Add(LstParameters(I))
                    End If
                Next
                Return DS
            Catch ex As Exception
                Throw New Exception(ex.Message)
            Finally
                DS.Dispose()
                If FCloseConnection Then
                    'Cerramos la conexión a la base de datos.
                    If Not FConnection.mCerrarBasedeDatos Then
                        'Si no se abre, recorremos los errores y lanzamos la excepción
                        If FConnection.mCantidadErrores > 0 Then
                            Dim Errores As String = String.Empty
                            For I As Integer = 1 To CInt(FConnection.mCantidadErrores - 1)
                                If Trim(FConnection.mDescripcionError(I)) <> String.Empty Then
                                    Errores = Errores & FConnection.mDescripcionError(I).ToString & vbCrLf
                                End If
                            Next
                            If Trim(Errores) <> String.Empty Then
                                Throw New Exception(Errores)
                            End If
                        End If
                    End If
                End If
            End Try
        End Function
        Public Function GetDataSet(ByVal ConnectionString As String, ByVal EsCifrada As Boolean) As DataSet
            Dim DS As New DataSet
            Try
                'Abrimos la conexión a la base de datos.
                If Not FConnection.bolAbrio Then
                    'If Not FConnection.mAbrirBasedeDatos(ConnectionString) Then
                    If Not FConnection.mAbrirBasedeDatos(ConnectionString, EsCifrada) Then
                        'Si no se abre, recorremos los errores y lanzamos la excepción
                        If FConnection.mCantidadErrores > 0 Then
                            Dim Errores As String = String.Empty
                            For I As Integer = 1 To CInt(FConnection.mCantidadErrores - 1)
                                If Trim(FConnection.mDescripcionError(I)) <> String.Empty Then
                                    Errores = Errores & FConnection.mDescripcionError(I).ToString & vbCrLf
                                End If
                            Next
                            If Trim(Errores) <> String.Empty Then
                                Throw New Exception(Errores)
                            End If
                        End If
                    End If
                End If

                Dim LstParameters As New List(Of SqlClient.SqlParameter)
                For I As Integer = 0 To Command.Parameters.Count - 1
                    LstParameters.Add(New SqlParameter(Command.Parameters(I).ParameterName, Command.Parameters(I).SqlDbType, Command.Parameters(I).Size))
                    LstParameters(LstParameters.Count - 1).Value = Command.Parameters(I).Value
                    LstParameters(LstParameters.Count - 1).Direction = Command.Parameters(I).Direction
                Next
                FConnection.mRST(FProcedure_Name, LstParameters.ToArray)

                If FConnection.mCantidadErrores > 0 Then
                    Dim Errores As String = String.Empty
                    For I As Integer = 1 To CInt(FConnection.mCantidadErrores - 1)
                        If Trim(FConnection.mDescripcionError(I)) <> String.Empty Then
                            Errores = Errores & FConnection.mDescripcionError(I).ToString & vbCrLf
                        End If
                    Next
                    If Trim(Errores) <> String.Empty Then
                        Throw New Exception(Errores)
                    End If
                End If
                DS = FConnection.GetDataSet
                OutputValues = New List(Of Object)
                For I As Integer = 0 To LstParameters.Count - 1
                    If LstParameters(I).Direction = ParameterDirection.Output Then
                        OutputValues.Add(LstParameters(I))
                    End If
                Next
                Return DS
            Catch ex As Exception
                Throw New Exception(ex.Message)
            Finally
                DS.Dispose()
                If FCloseConnection Then
                    'Cerramos la conexión a la base de datos.
                    If Not FConnection.mCerrarBasedeDatos Then
                        'Si no se abre, recorremos los errores y lanzamos la excepción
                        If FConnection.mCantidadErrores > 0 Then
                            Dim Errores As String = String.Empty
                            For I As Integer = 1 To CInt(FConnection.mCantidadErrores - 1)
                                If Trim(FConnection.mDescripcionError(I)) <> String.Empty Then
                                    Errores = Errores & FConnection.mDescripcionError(I).ToString & vbCrLf
                                End If
                            Next
                            If Trim(Errores) <> String.Empty Then
                                Throw New Exception(Errores)
                            End If
                        End If
                    End If
                End If
            End Try
        End Function

        Public Sub Execute(Optional ByVal CommandTimeout As Integer? = Nothing)
            Try
                'Abrimos la conexión a la base de datos.
                If Not FConnection.bolAbrio Then
                    'If Not FConnection.mAbrirBasedeDatos(ConnectionString) Then
                    If Not FConnection.mAbrirBasedeDatos(clsUtils.DSN, False) Then
                        'Si no se abre, recorremos los errores y lanzamos la excepción
                        If FConnection.mCantidadErrores > 0 Then
                            Dim Errores As String = String.Empty
                            For I As Integer = 1 To CInt(FConnection.mCantidadErrores - 1)
                                If Trim(FConnection.mDescripcionError(I)) <> String.Empty Then
                                    Errores = Errores & FConnection.mDescripcionError(I).ToString & vbCrLf
                                End If
                            Next
                            If Trim(Errores) <> String.Empty Then
                                Throw New Exception(Errores)
                            End If
                        End If
                    End If
                End If

                Dim LstParameters As New List(Of SqlClient.SqlParameter)
                For I As Integer = 0 To Command.Parameters.Count - 1
                    LstParameters.Add(New SqlParameter(Command.Parameters(I).ParameterName, Command.Parameters(I).SqlDbType, Command.Parameters(I).Size))
                    LstParameters(LstParameters.Count - 1).Value = Command.Parameters(I).Value
                    LstParameters(LstParameters.Count - 1).Direction = Command.Parameters(I).Direction
                Next

                FConnection.mExecute(FProcedure_Name, LstParameters.ToArray, CommandTimeout)
                If FConnection.mCantidadErrores > 0 Then
                    Dim Errores As String = String.Empty
                    For I As Integer = 1 To CInt(FConnection.mCantidadErrores - 1)
                        If Trim(FConnection.mDescripcionError(I)) <> String.Empty Then
                            Errores = Errores & FConnection.mDescripcionError(I).ToString & vbCrLf
                        End If
                    Next
                    If Trim(Errores) <> String.Empty Then
                        Throw New Exception(Errores)
                    End If
                End If
                OutputValues = New List(Of Object)
                For I As Integer = 0 To LstParameters.Count - 1
                    If LstParameters(I).Direction = ParameterDirection.Output Then
                        OutputValues.Add(LstParameters(I))
                    End If
                Next
            Catch ex As Exception
                Throw New Exception(ex.Message)
            Finally
                If FCloseConnection Then
                    'Cerramos la conexión a la base de datos.
                    If Not FConnection.mCerrarBasedeDatos Then
                        'Si no se abre, recorremos los errores y lanzamos la excepción
                        If FConnection.mCantidadErrores > 0 Then
                            Dim Errores As String = String.Empty
                            For I As Integer = 1 To CInt(FConnection.mCantidadErrores - 1)
                                If Trim(FConnection.mDescripcionError(I)) <> String.Empty Then
                                    Errores = Errores & FConnection.mDescripcionError(I).ToString & vbCrLf
                                End If
                            Next
                            If Trim(Errores) <> String.Empty Then
                                Throw New Exception(Errores)
                            End If
                        End If
                    End If
                End If
            End Try
        End Sub
        Public Sub Execute(ByVal ConnectionString As String, ByVal EsCifrada As Boolean, Optional ByVal CommandTimeout As Integer? = Nothing)
            Try
                'Abrimos la conexión a la base de datos.
                If Not FConnection.bolAbrio Then
                    'If Not FConnection.mAbrirBasedeDatos(ConnectionString) Then
                    If Not FConnection.mAbrirBasedeDatos(ConnectionString, EsCifrada) Then
                        'Si no se abre, recorremos los errores y lanzamos la excepción
                        If FConnection.mCantidadErrores > 0 Then
                            Dim Errores As String = String.Empty
                            For I As Integer = 1 To CInt(FConnection.mCantidadErrores - 1)
                                If Trim(FConnection.mDescripcionError(I)) <> String.Empty Then
                                    Errores = Errores & FConnection.mDescripcionError(I).ToString & vbCrLf
                                End If
                            Next
                            If Trim(Errores) <> String.Empty Then
                                Throw New Exception(Errores)
                            End If
                        End If
                    End If
                End If

                Dim LstParameters As New List(Of SqlClient.SqlParameter)
                For I As Integer = 0 To Command.Parameters.Count - 1
                    LstParameters.Add(New SqlParameter(Command.Parameters(I).ParameterName, Command.Parameters(I).SqlDbType, Command.Parameters(I).Size))
                    LstParameters(LstParameters.Count - 1).Value = Command.Parameters(I).Value
                    LstParameters(LstParameters.Count - 1).Direction = Command.Parameters(I).Direction
                Next

                FConnection.mExecute(FProcedure_Name, LstParameters.ToArray, CommandTimeout)
                If FConnection.mCantidadErrores > 0 Then
                    Dim Errores As String = String.Empty
                    For I As Integer = 1 To CInt(FConnection.mCantidadErrores - 1)
                        If Trim(FConnection.mDescripcionError(I)) <> String.Empty Then
                            Errores = Errores & FConnection.mDescripcionError(I).ToString & vbCrLf
                        End If
                    Next
                    If Trim(Errores) <> String.Empty Then
                        Throw New Exception(Errores)
                    End If
                End If
                OutputValues = New List(Of Object)
                For I As Integer = 0 To LstParameters.Count - 1
                    If LstParameters(I).Direction = ParameterDirection.Output Then
                        OutputValues.Add(LstParameters(I))
                    End If
                Next
            Catch ex As Exception
                Throw New Exception(ex.Message)
            Finally
                If FCloseConnection Then
                    'Cerramos la conexión a la base de datos.
                    If Not FConnection.mCerrarBasedeDatos Then
                        'Si no se abre, recorremos los errores y lanzamos la excepción
                        If FConnection.mCantidadErrores > 0 Then
                            Dim Errores As String = String.Empty
                            For I As Integer = 1 To CInt(FConnection.mCantidadErrores - 1)
                                If Trim(FConnection.mDescripcionError(I)) <> String.Empty Then
                                    Errores = Errores & FConnection.mDescripcionError(I).ToString & vbCrLf
                                End If
                            Next
                            If Trim(Errores) <> String.Empty Then
                                Throw New Exception(Errores)
                            End If
                        End If
                    End If
                End If
            End Try
        End Sub
        Public Sub Dispose()
            Command.Dispose()
            OutputValues = Nothing
            FProcedure_Name = Nothing
            FCloseConnection = False
            If FCloseConnection Then
                FConnection = Nothing
            End If
            FCloseConnection = Nothing
        End Sub

    End Class
    '----------------------------------------------------------------------------------'

    '--Clase encargada de procesar los métodos de procedimientos almacenados de la base de datos--'

    Partial Public Class Sps
        Public Shared Function tblCargaTempDelete(Optional ByVal Connection As clsDAL = Nothing) As StoredProcedure
            Dim SP As New StoredProcedure("tblCargaTempDelete", Connection)
            Return SP
        End Function

        Public Shared Function tblCargaInsert(ByVal strUser As String, ByVal OUT_intError As Integer?, ByVal OUT_strError As String, Optional ByVal Connection As clsDAL = Nothing) As StoredProcedure
            Dim SP As New StoredProcedure("tblCargaInsert", Connection)
            SP.Command.Parameters.Add("@strUser", SqlDbType.NVarChar, 50).Value = IIf(Not strUser Is Nothing, strUser, DBNull.Value)
            SP.Command.Parameters.Add("@OUT_intError", SqlDbType.Int, 50).Value = IIf(Not OUT_intError Is Nothing, OUT_intError, DBNull.Value)
            SP.Command.Parameters("@OUT_intError").Direction = ParameterDirection.Output
            SP.Command.Parameters.Add("@OUT_strError", SqlDbType.VarChar, 255).Value = IIf(Not OUT_strError Is Nothing, OUT_strError, DBNull.Value)
            SP.Command.Parameters("@OUT_strError").Direction = ParameterDirection.Output

            Return SP
        End Function
        Public Shared Function tblCarga_SeleccionarDatos(ByVal strNumeroEmpresario As String, Optional ByVal Connection As clsDAL = Nothing) As StoredProcedure
            Dim SP As New StoredProcedure("tblCarga_SeleccionarDatos", Connection)
            SP.Command.Parameters.Add("@strNumeroEmpresario", SqlDbType.NVarChar, 50).Value = IIf(Not strNumeroEmpresario Is Nothing, strNumeroEmpresario, DBNull.Value)
            Return SP
        End Function


        Public Shared Function tblCarga_ValidarDatos(Optional ByVal Connection As clsDAL = Nothing) As StoredProcedure
            Dim SP As New StoredProcedure("tblCarga_ValidarDatos", Connection)
            Return SP
        End Function

        Public Shared Function tblCarga_ValidarDatos_Fecha(Optional ByVal Connection As clsDAL = Nothing) As StoredProcedure
            Dim SP As New StoredProcedure("tblCarga_ValidarDatos_Fecha", Connection)

            Return SP
        End Function

    End Class






    '---------------------------------------------------------------------------------------------'    

End Namespace