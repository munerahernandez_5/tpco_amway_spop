﻿Option Strict Off
Option Explicit Off
Imports System.Text
Imports System.IO
Imports System.Data.OleDb

Public Class clsUtils
    Shared FDSN As String
    Public Shared ReadOnly Property DSN As String
        Get
            Return FDSN
        End Get
    End Property

    Public Shared Sub CargarDSN1(ByVal IN_DSN As String)
        FDSN = IN_DSN
    End Sub

    Public Shared Sub RaiseException(ByVal Ex As Exception)
        If Ex.Message.Contains(vbCrLf) Then
            Dim O_Msg As String = String.Empty
            Dim I_Msg As String() = Ex.Message.Split(CChar(vbCrLf))
            For I As Integer = 0 To I_Msg.Count - 1
                If Not I_Msg(I).Contains("At procedure") Then
                    O_Msg += I_Msg(I) + vbCr
                End If
            Next
            Throw New Exception(O_Msg)
        Else
            Throw New Exception(Ex.Message)
        End If
    End Sub

End Class